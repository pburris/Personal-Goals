# Area for thoughts that just need a home

## April 31st, 2017
### The Beginning
This is just the first draft, I will have a few vague goals as I develop this repo.

1. Get this in order -- Get my 2017 goals in line and go back through all of my other 'lists of things to do this year' so I can compile a real roadmap
2. Write more -- I want to document this whole process on [my blog](https://www.nitrobrush.com) -- A lot of the stuff will come from these lists
3. Web Dev projects -- See point 1


#### Ideas
Free-for-all section with ideas

1. First and foremost is getting into shape! I also need to start documenting this process
2. Electronics - build stuff, like [this LCD box](https://www.youtube.com/watch?v=hZRL8luuPb8)
3. [Build an 8-bit computer](https://www.youtube.com/watch?v=HyznrdDSSGM&list=PLowKtXNTBypGqImE405J2565dvjafglHU&index=1)
4. Get better with C and start using C++
5. Get better with Python
6. Get better with and start using ML
7. Get better at product design
8. Get a viola and start playing again
9. Learn a new musical instrument
