# Personal-Goals

This is where I will keep my goals. This idea was taken from a [talk given by Una Kravets](https://www.youtube.com/watch?v=xQEU0ZsvXYI) and will parrellel her [personal-goals](https://github.com/una/personal-goals) repository, with my own flavor.

# Overarching goals for 2017

1. Physical fitness - I need to get in shape
2. Programming paradigms/design patterns
3. Write more
4. Work on Python

# December 2017

1. Rethink this project
2. Structure long-term goals (1 year, 5 years)
3. Structure the short-term goals (split 1st year into 1 section per month)
