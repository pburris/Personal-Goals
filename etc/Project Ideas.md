# Project Ideas

1. Build a WYSYWIG
2. [100 Days of Algorithms](https://medium.com/100-days-of-algorithms/100-days-of-algorithms-challenge-41996f7e1ec8)
3. Create Gutenberg Reader app
4. Instantgram project/book (maybe use the Gutenberg Reader project for this instead?)
5. Create a Chrome extension to manage all of my bookmarks
6. Set up a mail client with something like [Postal](https://github.com/atech/postal)
7. Create something with text -> speech, speech -> text
8. Build a command line card game using elixir
9. Do Mackenzie Child's 12 in 12 challenge with Phoenix
10. Finish building the html/css/js syntax highlighter
11. Create my own LTC wallet using [Lcoin](https://github.com/bcoin-org/lcoin)
12. [Build a muon detector](https://news.mit.edu/2017/handheld-muon-detector-1121) and then use it to build a reak random number generator service
13. [Build and deploy a flask+react app](https://testdriven.io/part-one-intro/)
14. [Build a QR code reader](https://medium.freecodecamp.org/lets-enhance-how-we-found-rogerkver-s-1000-wallet-obfuscated-private-key-8514e74a5433) that can read broken codes
15. Write a movie script over the life of Madame Blavatsky and her impact on the modern world.
