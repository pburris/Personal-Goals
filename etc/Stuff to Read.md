# This is what I need to read!

> Remember to check the bookmarks out for articles to read!

  1. [ ] [Learning JavaScript Design Patterns](https://addyosmani.com/resources/essentialjsdesignpatterns/book/)
  2. [ ] [JavaScript Allongé](https://leanpub.com/javascriptallongesix/read)
  3. [ ] [Beej's Guide to Network Programming](http://beej.us/guide/bgnet/output/html/singlepage/bgnet.html)
