# Learning List

## Topics

  1. Node-GYP


## Courses

#### Udemy
  1. ~~[Ethereum application design](https://www.udemy.com/ethereum-dapp/learn/v4/overview) [100%]~~
  2. [After Effects](https://www.udemy.com/aftereffects/learn/v4/overview) [29%]
  3. ~~[Node/React Fullstack](https://www.udemy.com/node-with-react-fullstack-web-development/learn/v4/) [100%]~~
  4. [Complete React Native](https://www.udemy.com/the-complete-react-native-and-redux-course/learn/v4/overview) [0%]
  5. [Advanced React Native](https://www.udemy.com/react-native-advanced/learn/v4/overview) [0%]
  6. [Go: Complete Developer's Guide](https://www.udemy.com/go-the-complete-developers-guide/learn/v4/overview) [0%]
  7. ~~[Python for Data Science and ML](https://www.udemy.com/python-for-data-science-and-machine-learning-bootcamp/learn/v4/overview) [100%]~~
  8. ~~[Python Data Structures](https://www.udemy.com/python-for-data-structures-algorithms-and-interviews/learn/v4/overview) [100%]~~
  9. [Python Keras](https://www.udemy.com/zero-to-deep-learning/learn/v4/overview) [0%]
  10. [Advanced Unix](https://www.udemy.com/advance-unix-commands/learn/v4/overview) [0%]
  
  
#### Nodeschool.io

  1. [Bytewiser](https://github.com/maxogden/bytewiser)
  2. ~~[Currying](https://github.com/kishorsharma/currying-workshopper)~~
  3. [Performance](https://github.com/bevacqua/perfschool)
  4. [Debugging](https://github.com/joyent/node-debug-school)
  5. [CouchDB](https://github.com/robertkowalski/learnyoucouchdb)
  6. [Torrential](https://github.com/No9/torrential)
  7. [Web Audio](https://github.com/mmckegg/web-audio-school)
  8. [Native Node Addons](https://github.com/workshopper/goingnative)
  9. [Learn UV](https://github.com/thlorenz/learnuv)
  10. [Intro to WebGL with Three.js](https://github.com/alexmackey/IntroToWebGLWithThreeJS)
  11. [WebGL Workshop](https://github.com/stackgl/webgl-workshop)
