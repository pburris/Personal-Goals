# Accomplishments

This is where I will review each week, month and year. This is a retrospective where I briefly summarize
what I did and didn't do for the week. This space is for reviewing and tailoring my plan to make sure
that I stay on a realistic track, while still dreaming big.
