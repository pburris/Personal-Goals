# AI and ML

Links for AI and ML that I want to look through, or have already looked through.

  - [ ] [AI Company Content](https://www.aicompany.co/content-learning/)
  - [ ] [AI Bible](https://static1.squarespace.com/static/58404a909f74562842ba2dc0/t/59043f3937c581913d1204d8/1493450564624/AIcompany+Bible.pdf)
  - [ ] [Google Developers: Machine Learning Recipes Series](https://www.youtube.com/watch?v=cKxRvEZd3Mw)
  - [ ] [Sentdex: Machine Learning with Python](https://www.youtube.com/playlist?list=PLQVvvaa0QuDfKTOs3Keq_kaG2P55YRn5v)
  - [ ] [Sentdex: Natural Language Processing With Python and NLTK](https://www.youtube.com/watch?v=FLZvOKSCkxY&list=PLQVvvaa0QuDf2JswnfiGkliBInZnIC4HL)
