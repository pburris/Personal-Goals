# Cheat Sheets

This is where I put all the 'cheat-sheets' I find

1. [Rico Sta. Cruz's Cheat Sheets](http://ricostacruz.com/cheatsheets/)
2. [Dave Child's Cheat Sheets](https://www.cheatography.com/davechild/cheat-sheets/)
3. [React Cheat Sheet](https://reactcheatsheet.com/)
4. [Redux Cheat Sheet](http://ricostacruz.com/cheatsheets/redux.html)
5. [PHP Cheat Sheet](https://www.cheatography.com/davechild/cheat-sheets/php/)
6. [ES6 Cheat Sheet](https://github.com/DrkSephy/es6-cheatsheet)
7. [Ruby on Rails Cheat Sheet](https://www.cheatography.com/davechild/cheat-sheets/ruby-on-rails/)
8. [Node Cheat Sheet](https://gist.github.com/LeCoupa/985b82968d8285987dc3)
9. [Machine Learning Cheat Sheet](https://github.com/soulmachine/machine-learning-cheat-sheet)
10. [Docker Cheat Sheet](https://github.com/wsargent/docker-cheat-sheet)
