# CSS Bookmarks

## ToC

  1. [Animations](#animations)
  2. [SVG](#svg)
  3. [Theory](#theory)
  4. [Frameworks](#frameworks)
  5. [Flexbox](#flexbox)
  6. [Grid](#grid)
  7. [Transforms and Shapes](#transforms-and-shapes)
  8. [Tips and Tricks](#tips-and-tricks)
  9. [Tutorials](#tutorials)
  10. [Resources](#resources)
  11. [Typography](#typography)
  
## Animations

  - [ ] [The CSS Animations Pocket Guide](http://cssanimationspocketguide.com/?mc_cid=62b35866ca&mc_eid=90051662f8)
  - [x] [Alien Invasions, SVGs and CSS Animations](https://journal.helabs.com/alien-invasions-svgs-and-css-animations-d56c4d757209)
  - [ ] [Upgrading CSS Animation With Motion Curves](https://www.smashingmagazine.com/2016/08/css-animations-motion-curves/)
  - [ ] [20 stunning examples of CSS3 animation](http://www.creativebloq.com/features/22-stunning-examples-of-css3-animation)
  - [ ] [Transition Effect with CSS Masks](https://tympanus.net/codrops/2016/09/29/transition-effect-with-css-masks/)
  - [ ] [Bringing Pages to Life with the Web Animations API](https://www.sitepoint.com/bringing-pages-to-life-web-animations-api/)
  - [ ] [Introduction to SVG animation](https://www.oreilly.com/learning/introduction-to-svg-animation)
  - [ ] [Animation: Jump-through](https://medium.com/google-developers/animation-jump-through-861f4f5b3de4)
  - [ ] [Scaling Responsive Animations](https://css-tricks.com/scaling-responsive-animations/)
  - [ ] [How to Use CSS Animations Like a Pro](https://stories.jotform.com/how-to-use-css-animations-like-a-pro-dfacc1e97338)
  - [ ] [Performant CSS animation for beginners](http://codepen.io/IanHazelton/pen/GrPEaK)
  - [x] [Animista](http://animista.net/)
  - [ ] [Using CSS Transitions on Auto Dimensions](https://css-tricks.com/using-css-transitions-auto-dimensions)
  - [ ] [15 Inspiring Examples of CSS Animation on CodePen](https://webdesign.tutsplus.com/articles/15-inspiring-examples-of-css-animation-on-codepen--cms-23937)
  - [ ] [10 Examples of Animation on CodePen You Can Learn From](https://webdesign.tutsplus.com/articles/10-examples-of-animation-on-codepen-you-can-learn-from--cms-28244)
  - [ ] [Interpolation in CSS without animation](https://madebymike.com.au/writing/interpolation-without-animation/)
  - [ ] [A Comparison of Animation Technologies](https://css-tricks.com/comparison-animation-technologies/)
  - [ ] [How fast should your UI animations be?](http://valhead.com/2016/05/05/how-fast-should-your-ui-animations-be/)
  - [ ] [The Illusion Of Life: An SVG Animation Case Study](https://www.smashingmagazine.com/2016/07/an-svg-animation-case-study/)
  - [ ] [SVG Line Animation for the Uninitiated](https://medium.com/bitmatica-lab/svg-line-animation-for-the-uninitiated-5a65d91c6044)
  - [ ] [Animating your hero header](https://cssanimation.rocks/animating-hero-header/)
  - [ ] [Render 2016 - Val Head: Designing Meaningful Animation](https://vimeo.com/165995133)
  
## SVG

  - [ ] [Make a Morphing Play-Pause Button for HTML5 Video with SVG](http://thenewcode.com/1112/Make-a-Morphing-Play-Pause-Button-for-HTML5-Video-with-SVG)
  - [ ] [Practical SVG](https://alistapart.com/article/practical-svg)
  - [ ] [10 golden rules for responsive SVGs](http://www.creativebloq.com/how-to/10-golden-rules-for-responsive-svgs)
  - [ ] [The SVG `path` Syntax: An Illustrated Guide](https://css-tricks.com/svg-path-syntax-illustrated-guide/)
  - [ ] [‘Reskinnable’ SVG Symbols: How to Make Them (..and Why)](https://www.sitepoint.com/reskinnable-svg-symbols-how-to-make-them-and-why/)
  - [ ] [Align SVG Icons to Text and Say Goodbye to Font Icons](https://blog.prototypr.io/align-svg-icons-to-text-and-say-goodbye-to-font-icons-d44b3d7b26b4)
  - [ ] [Randomizing SVG Shapes](http://mediatemple.net/blog/tips/randomizing-svg-shapes/)
  - [ ] [SVG Vector Effects](http://callmenick.com/post/svg-vector-effects)
  - [ ] [Making SVGs Responsive with CSS](https://tympanus.net/codrops/2014/08/19/making-svgs-responsive-with-css/)
  - [ ] [Hero Patterns](http://www.heropatterns.com/)
  - [ ] [Nova Pattern](https://www.novapattern.com/)
  - [ ] [Map Rollovers](http://mediatemple.net/blog/tips/map-rollovers/)
  
## Theory

  - [ ] [Thoughtful CSS Architecture](https://seesparkbox.com/foundry/thoughtful_css_architecture)
  - [ ] [RSCSS: Reasonable System for CSS](http://rscss.io/)
  - [ ] [FORC: The Fear of Removing CSS](https://x-team.com/blog/forc-fear-of-removing-css/)
  - [ ] [Starting a Refactor with CSS Dig](https://css-tricks.com/starting-a-refactor-with-css-dig/)
  - [ ] [CSS Evolution: From CSS, SASS, BEM, CSS Modules to Styled Components](https://m.alphasights.com/css-evolution-from-css-sass-bem-css-modules-to-styled-components-d4c1da3a659b)
  - [x] [CSS and Scalability](http://mrmrs.io/writing/2016/03/24/scalable-css/)
  - [x] [React Functional CSS Tips](https://github.com/chibicode/react-functional-css-protips)
  - [x] [The invisible parts of CSS](https://madebymike.com.au/writing/the-invisible-parts-of-CSS/)
  - [ ] [Meaningful CSS: Style Like You Mean It](https://alistapart.com/article/meaningful-css-style-like-you-mean-it)
  - [ ] [Architecting Front-end Styles](https://robots.thoughtbot.com/architecting-front-end-styles)
  - [ ] [CSS coding techniques](https://hacks.mozilla.org/2016/05/css-coding-techniques/)
  - [x] [MaintainableCSS](http://maintainablecss.com/)
  - [x] [CSS Guidelines](http://cssguidelin.es/)
  - [ ] [CSS structure & anti patterns](https://blog.stapps.io/css-structure-anti-patterns-part1/)
  - [ ] [Leveling up in CSS](https://medium.freecodecamp.com/leveling-up-css-44b5045a2667)
  - [ ] [CSS Concerns](https://snook.ca/archives/html_and_css/css-concerns)
  - [ ] [Developing Extensible HTML and CSS Components](https://css-tricks.com/developing-extensible-html-css-components/)
  - [ ] [Ways To Reduce Content Shifting On Page Load](https://www.smashingmagazine.com/2016/08/ways-to-reduce-content-shifting-on-page-load/)
  
## Frameworks

  - [ ] [Expressive CSS](http://johnpolacek.github.io/expressive-css/)

## Flexbox

  - [ ] [Flexbox Patterns](http://www.flexboxpatterns.com/home)
  - [ ] [Even more about how Flexbox works — explained in big, colorful, animated gifs](https://medium.freecodecamp.com/even-more-about-how-flexbox-works-explained-in-big-colorful-animated-gifs-a5a74812b053)

## Grid

  - [ ] [variations on a grid](http://labs.jensimmons.com/2017/01-003.html)
  - [ ] [CSS Grid Reference](https://tympanus.net/codrops/css_reference/grid/)

## Transforms and Shapes

  - [ ] [A Redesign with CSS Shapes](https://alistapart.com/article/redesign-with-css-shapes)
  - [ ] [Things to Watch Out for When Working with CSS 3D](https://css-tricks.com/things-watch-working-css-3d/)
  - [ ] [Sloped edges with consistent angle in CSS](https://kilianvalkhof.com/2017/design/sloped-edges-with-consistent-angle-in-css)
  - [ ] [Creating Non-Rectangular Headers](https://css-tricks.com/creating-non-rectangular-headers/)
  - [ ] [How to make clouds with CSS 3D](https://www.clicktorelease.com/blog/how-to-make-clouds-with-css-3d/)
  - [ ] [Building a CSS Grid Overlay](https://css-tricks.com/building-css-grid-overlay/)
  - [ ] [Front-End Challenge Accepted: CSS 3D Cube](https://www.smashingmagazine.com/2016/07/front-end-challenge-accepted-css-3d-cube/)

## Tips and Tricks

  - [ ] [Quick Tip: Using CSS Counters to Style Incremental Elements](https://webdesign.tutsplus.com/tutorials/quick-tip-using-css-counters-to-style-incremental-elements--cms-23497)
  - [ ] [Full Width Containers in Limited Width Parents](https://css-tricks.com/full-width-containers-limited-width-parents/)
  - [ ] [Fun with CSS: NBA edition](https://www.chenhuijing.com/blog/progressive-enhancement-experiment/#👾)
  - [ ] [Responsive table layout](http://allthingssmitty.com/2016/10/03/responsive-table-layout/)
  - [ ] [Magic randomisation with nth-child and Cicada Principle](http://www.lottejackson.com/learning/nth-child-cicada-principle)
  - [ ] [Style List Markers in CSS](https://css-tricks.com/style-list-markers-css/)
  - [ ] [Media Query Breaks Modularity, Use Container Query](http://d6u.github.io/react-container-query/)
  - [ ] [Improving the Quality of Your CSS with PostCSS](https://www.sitepoint.com/improving-the-quality-of-your-css-with-postcss/)
  - [ ] [Relational and Attribute Selectors in CSS3](https://www.sitepoint.com/relational-and-attribute-selectors-in-css3/)
  - [ ] [An Ultimate Guide To CSS Pseudo-Classes And Pseudo-Elements](https://www.smashingmagazine.com/2016/05/an-ultimate-guide-to-css-pseudo-classes-and-pseudo-elements/)
  - [ ] [Tips for Aligning Icons to Text](https://css-tricks.com/tips-aligning-icons-text/)
  - [ ] [Sticky Footer, Five Ways](https://css-tricks.com/couple-takes-sticky-footer/)
  - [ ] [The :target Trick](https://bitsofco.de/the-target-trick/)
  - [ ] [Making IFrames Responsive](https://bitsofco.de/iframe-responsive/)
  - [ ] [Sticky headers](https://adactio.com/journal/10877)


## Tutorials

  - [ ] [CSS sidebar toggle](https://silvestarbistrovic.from.hr/en/articles/css-sidebar-toggle/)
  - [ ] [An Introduction to the Basics of Modern CSS Buttons](https://www.sitepoint.com/modern-css-buttons/)

## Resources

  - [ ] [CSS Filters](http://www.cssfilters.co/)
  - [ ] [You Might Not Need JavaScript](http://youmightnotneedjs.com/)
  - [ ] [SpinThatShit - Loaders and Spinners](https://matejkustec.github.io/SpinThatShit/)
  - [ ] [CSS Reference](http://cssreference.io/)
  - [ ] [Let’s Look at 50+ Interesting CSS Properties & Values](https://css-tricks.com/lets-look-50-interesting-css-properties-values/)
  - [ ] [ReactDOM: Best CSS books in 2017](https://reactdom.com/blog/css-books)
  - [ ] [How well do you know CSS display?](https://www.chenhuijing.com/blog/how-well-do-you-know-display)
  - [ ] [CSS Icon](http://cssicon.space/#/)
  - [ ] [CSS Only Loaders](https://codepen.io/IvanKhartov/pen/KmgzpX)

## Typography

  - [ ] [font-feature-settings](https://css-tricks.com/almanac/properties/f/font-feature-settings/)
  - [ ] [CSS Writing Modes](https://24ways.org/2016/css-writing-modes/)
  - [ ] [Molten Leading in CSS](https://css-tricks.com/molten-leading-css/)



  
  
