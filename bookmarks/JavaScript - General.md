# JavaScript

General JS bookmarks, both frontend and backend

## ToC

  1. [Libraries](#libraries)
  2. [Node](#node)
  3. [Theory](#theory)
  4. [Patterns](#patterns)
  5. [Functional Programming](#functional-programming)
  6. [Tutorials](#tutorials)
  7. [Service Workers and PWAs](#service-workers-and-pwas)
  8. [Tips](#tips)
  9. [Resources](#resources)
  
## Libraries

  - [ ] [Immutable](https://facebook.github.io/immutable-js/)
  - [ ] [Grafi.js](http://grafijs.org/)
  - [ ] [Leaflet.js](http://leafletjs.com/)
  - [ ] [Kute.js](http://thednp.github.io/kute.js/)
  - [ ] [Progressbar.js](https://kimmobrunfeldt.github.io/progressbar.js/)
  - [ ] [Fuse.js](http://fusejs.io/)
  - [ ] [Tesseract.js](http://tesseract.projectnaptha.com/)
  - [ ] [SVG Charting Libraries](http://mediatemple.net/blog/tips/svg-charting-libraries/)
  - [ ] [Diæresis.js](http://curiositry.com/diaeresis/)
  - [ ] [Tone.js](https://tonejs.github.io/)
  - [ ] [Anime.js](http://anime-js.com/)
  - [ ] [Monetize.js](https://monetizejs.com/)
  
## Node

  - [ ] [Writing Native Node.js Modules](https://blog.risingstack.com/writing-native-node-js-modules/)
  - [ ] [Simple Image Upload With Express](http://blog.robertonodi.me/simple-image-upload-with-express/)
  - [ ] [Advanced Node.js Project Structure Tutorial](https://blog.risingstack.com/node-js-project-structure-tutorial-node-js-at-scale/)
  - [ ] [Before you bury yourself in packages, learn the Node.js runtime itself](https://medium.freecodecamp.com/before-you-bury-yourself-in-packages-learn-the-node-js-runtime-itself-f9031fbd8b69)
  - [ ] [The 80/20 Guide to Async/Await in Node.js](http://thecodebarbarian.com/80-20-guide-to-async-await-in-node.js.html)
  - [ ] [Web Scraping in Node.js with Multiple Examples](https://hackprogramming.com/web-scraping-in-node-js-with-multiple-examples/)
  - [ ] [Deploying Node.js Microservices to AWS using Docker](https://community.risingstack.com/deploying-node-js-microservices-to-aws-using-docker/)
  - [ ] [So you want to make a PostCSS plugin](https://css-tricks.com/want-make-postcss-plugin/)
  
## Theory

  - [ ] [Infinite Scrolling Best Practices](https://uxplanet.org/infinite-scrolling-best-practices-c7f24c9af1d)
  - [ ] [Making your JavaScript Pure](https://alistapart.com/article/making-your-javascript-pure)
  - [ ] [Writing Testable JavaScript](https://alistapart.com/article/writing-testable-javascript)
  - [ ] [The Future of Browser History](https://medium.freecodecamp.com/browserhistory-2abad38022b1)
  - [ ] [On Growing Object Oriented Software, Guided by Tests](https://medium.com/@TuckerConnelly/on-growing-object-oriented-software-guided-by-tests-e4dbe0702299)
  - [ ] [Object Oriented Tricks: #1 The Art of Command Query Separation](https://hackernoon.com/oo-tricks-the-art-of-command-query-separation-9343e50a3de0)
  - [ ] [Object Oriented Tricks: #2 Law of Demeter](https://hackernoon.com/object-oriented-tricks-2-law-of-demeter-4ecc9becad85)
  
## Patterns

  - [ ] [A Tale of Three Lists](https://github.com/getify/a-tale-of-three-lists)
  - [ ] [JavaScript Patterns Collection](http://shichuan.github.io/javascript-patterns/)
  - [ ] [Addy Osmani: Essential JS Design Patterns](https://github.com/addyosmani/essential-js-design-patterns)
  - [ ] [Do Factory: JavaScript Design Patterns](http://www.dofactory.com/javascript/design-patterns)
  - [ ] [Understanding Design Patterns in JavaScript](https://code.tutsplus.com/tutorials/understanding-design-patterns-in-javascript--net-25930)
  - [ ] [Coderwall: JavaScript Design Patterns](https://coderwall.com/p/w2rctq/javascript-design-patterns)
  - [ ] [Observing your web app](https://ericbidelman.tumblr.com/post/149032341876/observing-your-web-app)
  - [ ] [JavaScript Object Creation: Patterns and Best Practices](https://www.sitepoint.com/javascript-object-creation-patterns-best-practises/)
  - [ ] [JavaScript Design Patterns: The Singleton](https://www.sitepoint.com/javascript-design-patterns-singleton/)
  
## Functional Programming
 
  - [ ] [What Is Functional Programming?](http://blog.jenkster.com/2015/12/what-is-functional-programming.html)
  - [ ] [From Callback to Future -> Functor -> Monad](https://hackernoon.com/from-callback-to-future-functor-monad-6c86d9c16cb5)
  
## Tutorials
  - [ ] [Creating Wavescroll](https://css-tricks.com/creating-wavescroll/)
  - [ ] [How to write your own Virtual DOM](https://medium.com/@deathmood/how-to-write-your-own-virtual-dom-ee74acc13060)
  - [ ] [10 Tips and Tricks That Will Make You an npm Ninja](https://www.sitepoint.com/10-npm-tips-and-tricks/)
  - [ ] [Making a Simple Credit Card Validation Form](http://tutorialzine.com/2016/11/simple-credit-card-validation-form/)
  - [ ] [An Introduction to mo.js](https://css-tricks.com/introduction-mo-js/)
  - [ ] [GPU Animation: Doing It Right](https://www.smashingmagazine.com/2016/12/gpu-animation-doing-it-right/)
  - [ ] [Build a Custom CMS for a Serverless Static Site Generator](https://css-tricks.com/build-custom-cms-serverless-static-site-generator/)
  - [ ] [Introducing GraphicsJS, a Powerful Lightweight Graphics Library](https://www.sitepoint.com/introducing-graphicsjs-a-powerful-lightweight-graphics-library/)
  - [ ] [How to Build Your Own Progressive Image Loader](https://www.sitepoint.com/how-to-build-your-own-progressive-image-loader/)
  - [ ] [An Animated Intro to RxJS](https://css-tricks.com/animated-intro-rxjs/)
  - [ ] [Pong with SVG.js](https://css-tricks.com/pong-svg-js/)
  - [ ] [How to Implement Smooth Scrolling in Vanilla JavaScript](https://www.sitepoint.com/smooth-scrolling-vanilla-javascript/)
  - [ ] [Understanding ASTs by Building Your Own Babel Plugin](https://www.sitepoint.com/understanding-asts-building-babel-plugin/)
  - [ ] [Design and Build Your Own JavaScript Library: Tips & Tricks](https://www.sitepoint.com/design-and-build-your-own-javascript-library/)
  - [ ] [Build a Music Streaming App with Electron, React & ES6](https://www.sitepoint.com/music-streaming-app-electron-react-es6/)
  - [x] [Building a Vertical Timeline With CSS and a Touch of JavaScript](https://webdesign.tutsplus.com/tutorials/building-a-vertical-timeline-with-css-and-a-touch-of-javascript--cms-26528)
  
## Service Workers and PWAs

  - [ ] [Offline Storage for Progressive Web Apps](https://medium.com/dev-channel/offline-storage-for-progressive-web-apps-70d52695513c)
  - [ ] [Service Workers: an Introduction](https://developers.google.com/web/fundamentals/getting-started/primers/service-workers)
  - [ ] [Retrofit Your Website as a Progressive Web App](https://www.sitepoint.com/retrofit-your-website-as-a-progressive-web-app/)
  - [ ] [The Offline Cookbook](https://developers.google.com/web/fundamentals/instant-and-offline/offline-cookbook/)
  - [ ] [Instant-loading Offline-first (Progressive Web App Summit 2016)](https://www.youtube.com/watch?v=qDJAz3IIq18)
  - [ ] [Everything You Should Know About Progressive Web Apps](http://tutorialzine.com/2016/09/everything-you-should-know-about-progressive-web-apps/)
  - [ ] [Architecting a web app to “just work” offline](https://blog.superhuman.com/architecting-a-web-app-to-just-work-offline-part-1-8697f316c0eb)
  - [ ] [We built a PWA from scratch - This is what we learned](https://14islands.com/blog/2017/01/19/progressive-web-app-from-scratch/)
  - [ ] [“Offline First” with Service Worker (Building a PWA, Part 1)](https://bitsofco.de/bitsofcode-pwa-part-1-offline-first-with-service-worker/)
  - [ ] ["Instant Loading" with IndexedDB (Building a PWA, Part 2)](https://bitsofco.de/bitsofcode-pwa-part-2-instant-loading-with-indexeddb/)
  - [ ] [Push Notifications on the Web (Building a PWA, Part 3)](https://bitsofco.de/bitsofcode-pwa-part-3-push-notifications/)
  
## Tips

  - [ ] [Smooth Scroll](http://codepen.io/rleve/pen/iCbgy)
  - [ ] [Quick Tip: How JavaScript References Work](https://www.sitepoint.com/how-javascript-references-work/)
  - [ ] [The Basics of DOM Manipulation in Vanilla JavaScript](https://www.sitepoint.com/dom-manipulation-vanilla-javascript-no-jquery/)
  - [ ] [Controlling CSS Animations and Transitions with JavaScript](https://css-tricks.com/controlling-css-animations-transitions-javascript/)
  - [ ] [Quick Tip: Persist Checkbox Checked State after Page Reload](https://www.sitepoint.com/quick-tip-persist-checkbox-checked-state-after-page-reload/)
  - [ ] [Using the New ES6 Collections: Map, Set, WeakMap, WeakSet](https://www.sitepoint.com/using-the-new-es6-collections-map-set-weakmap-weakset/)
  
## Resources

  - [ ] [Unheap - A tidy repository of javascript plugins](http://www.unheap.com/)
  - [ ] [Sitepoint: This Week in JavaScript - 6 March 2017](https://www.sitepoint.com/community/t/this-week-in-javascript-6-march-2017/255822)
  - [ ] [Luke Crouch - Scary Javascript That knows if You've Been Bad or Good [ Thunder Plains 2016 ]](https://www.youtube.com/watch?v=0XDpJUhDTos)
  - [ ] [Plain JS](https://plainjs.com/)
  - [ ] [Building performant expand & collapse animations](https://developers.google.com/web/updates/2017/03/performant-expand-and-collapse)
  - [ ] [The Definitive Guide to Object Streams in Node.js](https://community.risingstack.com/the-definitive-guide-to-object-streams-in-node-js/)
  - [ ] [Vanilla List](http://www.vanillalist.com/)
  - [ ] [20+ Docs and Guides for Front-end Developers (11 editions so far)](https://www.sitepoint.com/20-docs-guides-front-end-developers-11/)
