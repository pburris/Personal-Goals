# Bookmarks

This is where I will store and catagorize bookmarks for various topics.

This will include:
- Blog posts
- Videos
- Books
- Manuals
- Documentation
- Other Lists
- Repositories
- References
- You get the idea

### First order of business

Before I can realistically start using this folder I need to add all of the bookmarks from my macbook and my desktop
over to this folder. What a daunting task, but atleast I can start check them off to make sure I have actually read
them.
