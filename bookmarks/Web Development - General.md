# Web Development

## ToC

  1. [Emails](#emails)
  2. [HTML](#html)
  3. [Performance](#performance)
  4. [Bots](#bots)
  5. [Misc](#misc)
  
## Emails

  - [ ] [MJML](https://mjml.io/)
  - [ ] [Jason Rodriguez - Learn Everything About Email](http://rodriguezcommaj.com/resources/)
  - [ ] [Mark Robbins | Modern CSS and interactive email | CSS Day 2016](https://vimeo.com/181481382)
  - [ ] [How We Did It: Building the New Tuts+ Email Templates](https://webdesign.tutsplus.com/articles/how-we-did-it-building-the-new-tuts-email-templates--cms-26680)
  - [ ] [Foundation for Emails 2](http://foundation.zurb.com/emails.html)
  
## HTML

  - [ ] [Quick Tip: The Right Way to Use Figure & Figcaption Elements](https://www.sitepoint.com/quick-tip-the-right-way-to-use-figure-and-figcaption-elements/)
  - [ ] [The State of HTML5 Input Elements](https://www.sitepoint.com/the-state-of-html5-input-elements/)
  - [ ] [Lazy Loading Images? Don’t Rely On JavaScript!](https://www.robinosborne.co.uk/2016/05/16/lazy-loading-images-dont-rely-on-javascript/)
  - [ ] [Random Interesting Facts on HTML/SVG usage](https://css-tricks.com/random-interesting-facts-htmlsvg-usage/)
  - [ ] [An Introduction To Building And Sending HTML Email For Web Developers](https://www.smashingmagazine.com/2017/01/introduction-building-sending-html-email-for-web-developers/)
  - [ ] [HTML Reference](http://htmlreference.io/)
  - [ ] [HTML APIs: What They Are And How To Design A Good One](https://www.smashingmagazine.com/2017/02/designing-html-apis/)
  - [ ] [Linting HTML using CSS](https://bitsofco.de/linting-html-using-css/)
  - [ ] [Smashing Semantic SEO in 2017 & Beyond: The Ultimate Guide](http://www.digitalcurrent.com/seo-engine-optimization/semantic-seo-guide/)
  - [ ] [Everything You Need to Know About HTML’s ‘pre’ Element](https://www.sitepoint.com/everything-need-know-html-pre-element/)

## Performance

  - [ ] [Optimising the front end for the browser](https://hackernoon.com/optimising-the-front-end-for-the-browser-f2f51a29c572)
  - [ ] [JavaScript Start-up Performance](https://medium.com/reloading/javascript-start-up-performance-69200f43b201)
  - [ ] [Caching Explained](https://cachingexplained.com/)
  - [ ] [Optimize With HTTP/2 Server Push and Service Workers!](https://blog.yld.io/2017/03/01/optimize-with-http-2-server-push-and-service-workers/#.WQ4etVPyuRu)
  - [ ] [Five CSS Performance Tools to Speed up Your Website](https://www.sitepoint.com/five-css-performance-tools-speed-website/)
  - [ ] [You Don't Get AMP](http://blog.153.io/2017/03/08/you-dont-get-amp/)
  - [ ] [A Comprehensive Guide To HTTP/2 Server Push](https://www.smashingmagazine.com/2017/04/guide-http2-server-push/)
  - [ ] [Debugging Tips and Tricks](https://css-tricks.com/debugging-tips-tricks/)
  - [ ] [Ways You Need To Tell The Browser How To Optimize](https://css-tricks.com/ways-need-tell-browser-optimize/)
  - [ ] [About that ‘mobile’ in Accelerated Mobile Pages](https://medium.com/@pbakaus/about-that-mobile-in-accelerated-mobile-pages-3802cc1b4644)
  - [ ] [Understanding the Critical Rendering Path](https://bitsofco.de/understanding-the-critical-rendering-path/)
  
## Bots

  - [ ] [Bot Stash: A curated directory of chat bot resources & tools](http://www.botsfloor.com/botstash/)
  - [ ] [Developer's Introduction To Chatbots](http://tutorialzine.com/2016/11/introduction-to-chatbots/)
  
## Misc
