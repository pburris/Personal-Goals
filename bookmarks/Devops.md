# Devops
Devops and related links.

Note: Docker links in Docker bookmarks file

- [ ] [Easy Deployment of PHP Applications with Deployer](https://www.sitepoint.com/deploying-php-applications-with-deployer/)
- [ ] [Visual Representation of SQL Joins](https://www.codeproject.com/Articles/33052/Visual-Representation-of-SQL-Joins)
- [ ] [Serverless Architectures](https://martinfowler.com/articles/serverless.html)
- [ ] [Explain Shell](https://explainshell.com/)
- [ ] [My First 10 Minutes On a Server ](https://www.codelitt.com/blog/my-first-10-minutes-on-a-server-primer-for-securing-ubuntu/)
- [ ] [Learn Chef](https://learn.chef.io/#/)
- [ ] [Linux Shell Scripting Tutorial (LSST) v2.0](https://bash.cyberciti.biz/guide/Main_Page)
- [ ] [How To Install Linux, Apache, MySQL, PHP (LAMP) stack on Ubuntu 14.04](https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-14-04)
- [ ] [Systemd Essentials: Working with Services, Units, and the Journal](https://www.digitalocean.com/community/tutorials/systemd-essentials-working-with-services-units-and-the-journal)
- [ ] [12 Factor App](https://12factor.net/)
- [ ] [Installing/Upgrading Drush on Ubuntu](https://www.drupal.org/node/1248790)
- [ ] [Stack Overflow: How can I monitor the progress of an import of a large .sql file?](https://dba.stackexchange.com/questions/17367/how-can-i-monitor-the-progress-of-an-import-of-a-large-sql-file)
- [ ] [How To Secure Nginx with Let's Encrypt on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-16-04)
