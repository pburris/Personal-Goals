# React, Redux, GraphQL
 
 
## ToC
 
 1. [React](#react)
 2. [Redux](#redux)
 3. [Misc](#misc)
 4. [Tutorials](#tutorials)
 5. [Testing](#testing)
 6. [Component Libraries](#component-libraries)
 7. [GraphQL](#graphql)
 
## React
 
 - [x] [Securing React Redux Apps With JWT Tokens](https://medium.com/@rajaraodv/securing-react-redux-apps-with-jwt-tokens-fcfe81356ea0)
 - [ ] [10 Tips for Better Redux Architecture](https://medium.com/javascript-scene/10-tips-for-better-redux-architecture-69250425af44#.7waudiszr)
 - [x] [Tutorial's Point ReactJS](https://www.tutorialspoint.com/reactjs/)
 - [ ] [Introducing React Reform](https://www.codecks.io/blog/2017/introducing-react-reform/)
 - [ ] [React Interview Questions](https://tylermcginnis.com/react-interview-questions/)
 - [ ] [Primer on the React Ecosystem](http://patternhatch.com/2016/07/06/a-primer-on-the-react-ecosystem-part-1-of-3/)
 - [ ] [React.js in patterns](http://krasimirtsonev.com/blog/article/react-js-in-design-patterns)
 - [ ] [React in patterns](https://github.com/krasimir/react-in-patterns)
 - [ ] [React patterns](http://reactpatterns.com/)
 - [ ] [Planning Center: React Patterns](https://github.com/planningcenter/react-patterns)
 - [ ] [Higher Order React Components](http://natpryce.com/articles/000814.html)
 - [ ] [Experimenting With Higher-Order Components In ReactJS](https://www.bennadel.com/blog/2888-experimenting-with-higher-order-components-in-reactjs.htm)
 - [ ] [Styled Theme: for style-components](https://github.com/diegohaz/styled-theme)
 - [ ] [Philosophy of React](https://code.likeagirl.io/the-philosophy-of-react-e2c126c61af3)
 - [ ] [All the Conditional Renderings in React](https://www.robinwieruch.de/conditional-rendering-react/)
 - [ ] [10 React Mini-Patterns](https://medium.com/@david.gilbertson/10-react-mini-patterns-c1da92f068c5#.3de2jd4jl)
 - [ ] [How to build animated microinteractions in React](https://medium.freecodecamp.com/how-to-build-animated-microinteractions-in-react-aab1cb9fe7c8#.9y0g62ltm)
 - [ ] [Submitting a Form in React](http://kadwill.com/submitting-a-form-in-react/)
 - [ ] [Routing in React the Uncomplicated Way](https://hackernoon.com/routing-in-react-the-uncomplicated-way-b2c5ffaee997)
 - [ ] [Build your own React Router v4](https://tylermcginnis.com/build-your-own-react-router-v4/)
 - [ ] [React Quickly: How to Work with Forms in React](https://www.sitepoint.com/work-with-forms-in-react/)
 - [ ] [Clear Form in Redux when Navigating with React-Router](https://ilonade.github.io/blog/clear-form-in-redux-when-navigating-with-react-router/)
 - [ ] [45% Faster React Functional Components, Now](https://medium.com/missive-app/45-faster-react-functional-components-now-3509a668e69f)
 - [ ] [React on the Server for Beginners: Build a Universal React and Node App](https://scotch.io/tutorials/react-on-the-server-for-beginners-build-a-universal-react-and-node-app?utm_source=mybridge&utm_medium=blog&utm_campaign=read_more)
 - [ ] [React.js pure render performance anti-pattern](https://medium.com/@esamatti/react-js-pure-render-performance-anti-pattern-fb88c101332f)
 - [x] [5 Types of React Application State](http://jamesknelson.com/5-types-react-application-state/)
 - [ ] [React Rendering Misconceptions](https://robots.thoughtbot.com/react-rendering-misconception)
 
## Redux

 - [x] [Adding A Robust Form Validation To React Redux Apps](https://medium.com/@rajaraodv/adding-a-robust-form-validation-to-react-redux-apps-616ca240c124)
 - [ ] [Idiomatic Redux: Thoughts on Thunks, Sagas, Abstraction, and Reusability](http://blog.isquaredsoftware.com/2017/01/idiomatic-redux-thoughts-on-thunks-sagas-abstraction-and-reusability/)
 - [ ] [Redux Step by Step: A Simple and Robust Workflow for Real Life Apps](https://hackernoon.com/redux-step-by-step-a-simple-and-robust-workflow-for-real-life-apps-1fdf7df46092#.cugck7brx)
 - [ ] [Ducks - Modular Redux](https://github.com/erikras/ducks-modular-redux)
 - [ ] [Practical Redux: 8 parts](http://blog.isquaredsoftware.com/2016/10/practical-redux-part-0-introduction/)
 - [ ] [Black Pixel Labs Redux Style Guide](http://bpxl-labs.github.io/redux-handbook/)
 - [ ] [Three Rules For Structuring (Redux) Applications](https://jaysoo.ca/2016/02/28/organizing-redux-application/)
 - [x] [The Anatomy of A React Redux App](https://medium.com/@rajaraodv/the-anatomy-of-a-react-redux-app-759282368c5a)
 - [ ] [Redux State Keys - A predictable yet dynamic substate](https://robinwieruch.de/redux-state-keys/)
 - [ ] [Encapsulating the Redux State Tree](http://randycoulman.com//blog/2016/09/13/encapsulating-the-redux-state-tree/)
 
## Misc

 - [x] [Webpack — The Confusing Parts](https://medium.com/@rajaraodv/webpack-the-confusing-parts-58712f8fcad9)
 - [ ] [Uncaught ReferenceError: webpackJsonp is not defined - Github Issue](https://github.com/webpack/webpack/issues/368)
 - [ ] [React/Redux Links](https://github.com/markerikson/react-redux-links)
 - [ ] [React/Redux Real World Example App](https://github.com/gothinkster/react-redux-realworld-example-app)
 - [ ] [React/Redux Login Flow](https://github.com/mxstbr/login-flow)
 - [ ] [Every time you build a to-do list app, a puppy dies](https://medium.freecodecamp.com/every-time-you-build-a-to-do-list-app-a-puppy-dies-505b54637a5d)
 - [ ] [Improving first time load of a Production React App](https://hackernoon.com/improving-first-time-load-of-a-production-react-app-part-1-of-2-e7494a7c7ab0#.ypsh5wtx1)
 - [ ] [Code Sandbox: Online React Playground](https://codesandbox.io/)
 - [ ] [React Training, from Tyler McGinnis](https://reacttraining.com/)
 - [ ] [Webpack Stats Analyzer](http://webpack.github.io/analyse/)
 - [ ] [Webpack Chart Analyzer](https://alexkuz.github.io/webpack-chart/)
 - [ ] [Webpack Visualizer](https://chrisbateman.github.io/webpack-visualizer/)
 
## Tutorials

 - [ ] [Building a Simple CRUD App with React + Redux](http://www.thegreatcodeadventure.com/building-a-simple-crud-app-with-react-redux-part-1/#table-of-contents)
 - [ ] [Test Driven React Tutorial](https://www.spencerdixon.com/blog/test-driven-react-tutorial.html)
 - [ ] [Build an Imgur app with React Native and MobX](https://school.shoutem.com/lectures/build-simple-imgur-client-react-native/)
 - [ ] [Render a Hearthstone Card Using React and SVG](https://hackernoon.com/render-a-hearthstone-card-using-react-and-svg-cb086957e74a)
 - [ ] [Build a Lunch Recommendation App with React Native](https://medium.com/react-native-development/build-a-lunch-recommendation-app-react-native-school-fdc05de70811)
 - [ ] [React + Flux Backed by Rails API](http://fancypixel.github.io/blog/2015/01/28/react-plus-flux-backed-by-rails-api/)
 - [ ] [Full-stack React + GraphQL Tutorial](https://dev-blog.apollodata.com/full-stack-react-graphql-tutorial-582ac8d24e3b)
 
## Testing

 - [ ] [Good Practices for Testing React Apps](https://medium.com/@TuckerConnelly/good-practices-for-testing-react-apps-3a64154fa3b1#.kjrosxyn8)
 - [ ] [Unit Testing a Redux App](https://www.codementor.io/reactjs/tutorial/redux-unit-test-mocha-mocking)
 - [ ] [TDD and React Components](https://medium.com/@nackjicholsonn/tdd-and-react-components-5ae5a9a5a7bf)
 - [ ] [Testing Redux Applications](http://randycoulman.com/blog/2016/03/15/testing-redux-applications/)
 - [ ] [Client Side Testing with Mocha and Karma](https://sean.is/writing/client-side-testing-with-mocha-and-karma/)
 - [ ] [Integration Testing Strategies](https://skillsmatter.com/skillscasts/8029-integration-testing-strategies-for-react-applications) - Video
 - [ ] [Automated Frontend Testing](http://rupl.github.io/frontend-testing/#/)
 - [ ] [Some Thoughts On Testing React/Redux Applications](https://medium.com/javascript-inside/some-thoughts-on-testing-react-redux-applications-8571fbc1b78f)
 - [ ] [Testing in React: Getting Off The Ground](https://medium.com/javascript-inside/testing-in-react-getting-off-the-ground-5f569f3088a)
 - [ ] [Egghead.io: React Testing Cookbook](https://egghead.io/courses/react-testing-cookbook)
 - [ ] [Testing React Components Best Practices](https://medium.com/selleo/testing-react-components-best-practices-2f77ac302d12)
 - [ ] [How to test React and Redux with Redux-saga and ReactDnD](https://medium.freecodecamp.com/testing-react-and-redux-with-redux-saga-and-reactdnd-whew-dedebcbd78dd)
 - [ ] [How to Test React Components Using Jest](https://www.sitepoint.com/test-react-components-jest)
 
## Component Libraries

 - [ ] [Ant Design React Components](https://ant.design/docs/react/introduce)
 - [ ] [Victory - charting library](http://formidable.com/open-source/victory/)
 - [ ] [Recharts - charting library](http://recharts.org/#/en-US)
 
## GraphQL

 - [ ] [Thinking in Relay](https://facebook.github.io/relay/docs/thinking-in-relay.html#content)
 - [ ] [The next step for realtime data in GraphQL](https://dev-blog.apollodata.com/the-next-step-for-realtime-data-in-graphql-b564b72eb07b#.fmqmyxbxy)
 - [ ] [React + GraphQL = ❤](https://medium.com/@FdMstri/react-graphql-e0c1ca714525#.yk96qklmn)
 - [ ] [Explaining GraphQL Connections](https://dev-blog.apollodata.com/explaining-graphql-connections-c48b7c3d6976#.o49fzzxzc)
 - [ ] [Getting started with Redux and GraphQL](https://medium.com/@childsmaidment/getting-started-with-redux-and-graphql-8384b3b25c56)
 - [ ] [Apollo GraphQL client](http://dev.apollodata.com/)
 - [ ] [Designing GraphQL Mutations](https://dev-blog.apollodata.com/designing-graphql-mutations-e09de826ed97)
 - [ ] [Learn Relay (.org)](https://www.learnrelay.org/overview/intro/)
 - [ ] [Five Ways Vulcan Makes Apollo Even Better](https://blog.vulcanjs.org/five-ways-vulcan-makes-apollo-even-better-c4625dd2288)
 - [ ] [The Anatomy of a GraphQL Query](https://dev-blog.apollodata.com/the-anatomy-of-a-graphql-query-6dffa9e9e747)
 
